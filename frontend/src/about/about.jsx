import React from 'react'
import PageHeader from "../template/pageHeader";

export default props => (
    <div>
        <PageHeader name="Sobre" small="Nós"/>
        <h2>Nossa História</h2>
        <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Obcaecati ea voluptatem necessitatibus? Quas quisquam sapiente magni, laboriosam fuga facere culpa aliquam, optio, iure eos natus soluta eum excepturi tenetur modi!</p>
        <h2>Missão e Visão</h2>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Illum quos excepturi iste id accusamus accusantium quo qui architecto odit maiores, repellat numquam explicabo, nemo ad nesciunt soluta, in ducimus nostrum.</p>
    </div>
)